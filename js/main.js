import { getStateByCEP } from './modules/cep.js'
import { getCovid19Information } from './modules/covid-19.js'
import { CEP } from './modules/form.js'


const searchForm = document.querySelector('#search-form')

const errorMessage = document.querySelector('#search-form .error-message')

const cepInput = document.querySelector('#search-form input[name=cep]')

const tableBody = document.querySelector('#cases-information tbody')


const CEPValidator = new CEP(cepInput)


function
updateCasesTable(casesInfo)
{
    tableBody.innerHTML = `
        <tr>
            <td>${casesInfo['uf']}</td>
            <td>${casesInfo['state']}</td>
            <td>${casesInfo['cases']}</td>
            <td>${casesInfo['suspects']}</td>
            <td>${casesInfo['refuses']}</td>
            <td>${casesInfo['datetime']}</td>
        </tr>
    `
}

function
manageSearchForm(e)
{
    e.preventDefault()

    const cepField = e.target.cep

    errorMessage.innerText = ''

    if ([...cepField.classList].indexOf('errorInput') != -1) return

    const cep = cepField.value

    return getStateByCEP(cep)
        .then(json => {
            if (typeof(json['erro']) !== 'undefined') throw `Error finding CEP ${cep}`

            return getCovid19Information(json['uf'])
        })
        .then(json => { updateCasesTable(json) })
        .catch(e => { errorMessage.innerText = e })

}

searchForm.addEventListener('submit', manageSearchForm)

cepInput.addEventListener('keyup', event => CEPValidator.mask())

cepInput.addEventListener('blur', event => CEPValidator.validate())
