function toggleFieldValidation(field, noMatch)
{
    if (field.className.indexOf('errorInput') == -1 && noMatch){
        field.classList.add('errorInput')
    }
    else if (!noMatch)
    {
        field.classList.remove('errorInput')
    }
}

export class Field
{
    constructor(field){
        this.field = field
    }

    mask() {}

    validate() {
        const match = this.field.value.match(this.validationRegex)

        toggleFieldValidation(this.field, match === null)
    }
}

export class CEP extends Field {
    validationRegex = /\d{5}\-\d{3}/

    mask()
    {
        this.field.value = (
            this.field.value
                .replace(/\D/g, '')
                .replace(/(\d{5})(\d)/, '$1-$2')
                .replace(/(\-\d{3})(.+)/, '$1')
        )
    }
}
