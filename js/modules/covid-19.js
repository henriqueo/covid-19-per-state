export
function
getCovid19Information(state)
{
    const url = `https://covid19-brazil-api.now.sh/api/report/v1/brazil/uf/${state}`

    return fetch(url)
        .then(r => { return r.json() })
}
