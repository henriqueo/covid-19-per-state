export
function
getStateByCEP(cep)
{
    cep = cep.replaceAll('-', '')

    const url = `https://viacep.com.br/ws/${cep}/json/`

    return fetch(url)
        .then(r => { return r.json() })
}
